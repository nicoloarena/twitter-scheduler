# User model

class User:

    def __init__(self, screen_name, id, name):
        self.__screen_name = screen_name
        self.__id = id
        self.__name = name

    def getScreenName(self):
        return self.__screen_name

    def getId(self):
        return self.__id

    def getName(self):
        return self.__name
