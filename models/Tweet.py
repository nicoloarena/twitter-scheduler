# model of a Tweet

class Tweet:

    def __init__(self, author_id, text, timestamp, id = None, tweet_id = None, created_at = None, scheduled_for = None):
        self.__id = id
        self.__tweet_id = tweet_id
        self.__author_id = author_id
        self.__text = text
        self.__timestamp = timestamp
        self.__created_at = created_at
        self.__scheduled_for = scheduled_for

    def getId(self):
        return self.__id

    def getTweetId(self):
        return self.__tweet_id

    def getAuthorId(self):
        return self.__author_id

    def getText(self):
        return self.__text

    def getTimestamp(self):
        return self.__timestamp

    def getCreatedAt(self):
        return self.__created_at

    def getScheduledFor(self):
        return self.__scheduled_for

    def __str__(self):
        when = ""
        if self.__scheduled_for is not None:
            when = " scheduled for " + str(self.__scheduled_for.date()) + " at " + str(self.__scheduled_for.time())
        elif self.__created_at is not None:
            when = " posted on " + str(self.__created_at.date()) + " at " + str(self.__created_at.time())
            if self.__tweet_id is not None:
                when += ", tweet id: " + self.__tweet_id
        return "\"" + self.__text + "\"" + when
