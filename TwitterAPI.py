# contains methods for Twitter API
import json
from datetime import timezone

import tweepy
import webbrowser
import os

from models.Tweet import Tweet


def oauth1():
    # oauth creation
    config_file = open("config.json", "r")
    config = json.load(config_file)
    config_file.close()
    auth = tweepy.OAuthHandler(config["consumer_key"], config["consumer_secret"], "oob")
    try:
        redirect_url = auth.get_authorization_url()
    except tweepy.TweepError:
        print('Error! Failed to get request token.')
        return None

    # disabling output of webbrowser.open()
    savout = os.dup(1), os.dup(2)
    os.close(1)
    os.close(2)
    os.open(os.devnull, os.O_RDWR)
    try:
        webbrowser.open(redirect_url)
    finally:
        os.dup2(savout[0], 1)
        os.dup2(savout[1], 2)

    verifier = input('Insert PIN: ')

    try:
        auth.get_access_token(verifier)
    except tweepy.TweepError:
        print('Error! Failed to get access token.')
        return None
    return tweepy.API(auth)


def getLoggedUser(api):
    try:
        screen_name = api.get_settings()["screen_name"]
        user = api.get_user(screen_name)
        return user
    except tweepy.error.TweepError as e:
        print("Error " + str(e.args[0][0]['code']) + ": " + e.args[0][0]['message'])


def getUserId(user):
    return user.id_str


def getUserScreenName(user):
    return user.screen_name


def getUserName(user):
    return user.name


def postTweet(api, text):
    # tweet publication or scheduling
    try:
        response = api.update_status(text)
        return response
    except tweepy.error.TweepError as e:
        print("Error " + str(e.args[0][0]['code']) + ": " + e.args[0][0]['message'])


def getPostedTweets(api, user_id):
    try:
        response = api.user_timeline(user_id = user_id, count = 200, include_rts = True)
        tweets = []
        for status in response:
            # converting from utc to local timezone
            localtz = status.created_at.astimezone()
            created_at_utc = status.created_at.replace(tzinfo = timezone.utc)
            created_at = created_at_utc.astimezone(localtz.tzinfo)

            tweets.append(Tweet(author_id = status.user.id_str, text = status.text, timestamp = None, id = None,
                                tweet_id = status.id_str, created_at = created_at, scheduled_for = None))
        return tweets
    except tweepy.error.TweepError as e:
        print("Error " + str(e.args[0][0]['code']) + ": " + e.args[0][0]['message'])
