# provides all functions for publishing, scheduling, retrieving published and scheduled tweets
import TwitterAPI
from datetime import datetime
import time

from main import commands, options
import Database
from models.Tweet import Tweet
from models.User import User

api = None
user_id = None
database = None


def login(full_command):
    if len(full_command) > 1:
        # error
        return False
    global api
    api = TwitterAPI.oauth1()
    if api is not None:
        print("Login successful.")
        global database
        database = Database.connect()
        user = TwitterAPI.getLoggedUser(api)
        if user is None:  # error
            return False
        global user_id
        screen_name, user_id, name = TwitterAPI.getUserScreenName(user), TwitterAPI.getUserId(user), \
                                     TwitterAPI.getUserName(user)
        user = User(screen_name, user_id, name)
        Database.saveUser(user)
        print("Welcome @" + screen_name + " !")
        return True
    else:
        return False


def postTweet(full_command):
    if api is None:
        print("You have to login first.")
        return False
    text = full_command[1].replace("\\n", "\n")
    if len(full_command) == 2:
        # post now

        # saving tweet to database
        id = __saveTweet(text)

        # posting tweet
        response = TwitterAPI.postTweet(api, text)

        # update tweet in db
        __updateTweet(response, id)

    elif len(full_command) == 4:
        # schedule. command: post "text" --datetime yyyy-mm-dd@hh:mm
        if len(text) >= 280:
            return False  # max length is 280 characters
        try:
            index = full_command.index(options["schedule"])
        except ValueError:
            return False  # error, invalid option
        d8time = full_command[index+1]

        # check datetime
        d8time = __checkDateTime(d8time)
        if not d8time:
            print("Error: invalid datetime.")
            return False

        # saving scheduled tweet to database
        id = __saveTweet(text, d8time)
        if id is not None:
            print("Tweet scheduled.")
            return True
        else:
            return False
    else:
        # error
        return False
    return True


def postScheduledDaemon():
    # this function checks scheduled tweets whose datetime has passed each minute and posts them

    while True:
        time.sleep(60)
        if api is None:  # user not logged yet
            continue
        tweets = Database.getScheduledTweets(user_id)
        for tweet in tweets:
            # post the tweet and update tweet_id in db

            # posting tweet
            response = TwitterAPI.postTweet(api, tweet.getText())

            # update tweet in db
            __updateTweet(response, tweet.getId())


def retrieveTweets(full_command):
    if api is None:
        print("You have to login first.")
        return False
    if len(full_command) != 1:
        return False
    tweets = TwitterAPI.getPostedTweets(api, user_id)
    if tweets is None:
        print("Error retrieving posted Tweets.")
        return False
    elif len(tweets) == 0:
        print("No posted Tweets.")
    else:
        print("Posted Tweets:")
        for tweet in tweets:
            print("    -  " + str(tweet).replace("\n", "\n        "))
    return True


def getFutureScheduled(full_command):
    if api is None:
        print("You have to login first.")
        return False
    if len(full_command) != 1:
        return False
    tweets = Database.getScheduledTweets(user_id, False)
    if len(tweets) == 0:
        print("No scheduled Tweets.")
    else:
        print("Scheduled Tweets:")
        for tweet in tweets:
            print("    -  " + str(tweet).replace("\n", "\n        "))
    return True


def support():
    print("Use: <command> [parameter] [option [value]]\n\n"
          "Commands:\n"
          "\t" + commands["login"] + " : use it to login to your twitter account. "
                                     "You have to authorize the app in the web browser and insert the pin here."
          "\n"
          "\t" + commands["post"] + " <\"text\"> : use it to create a new Tweet. You can insert #hashtags and "
                                    "@mentions, and add lines using \\n as newline character. "
                                    "The text must be enclosed in double quotes, his max length is 280 characters and "
                                    "it cannot be equal to the last Tweet posted. "
                                    "Add --datetime option to schedule the Tweet. "
                                    "You can only post 300 Tweets during a 3 hour period."
          "\n"
          "\t" + commands["posted"] + " : use it to view the most recent 200 posted tweets. "
                                      "Tweets are ordered by posting date and time, in descending order."
          "\n"
          "\t" + commands["scheduled"] + " : use it to view the closest 200 scheduled tweets. "
                                         "Tweets are ordered by scheduling date and time, in ascending order."
          "\n\n"
          "Options:\n"
          "\t" + options["schedule"] + " <YYYY-MM-DD@hh:mm> : specify when the tweet will be posted. "
                                       "In order: year, month, day, @, hour, minute. "
                                       "You have to keep the app running and be logged in order to post "
                                       "scheduled tweets."
          "\n"
          # "\t" + options["verbose"] + " : deactivate console confirmation outputs. You will still see errors."
    )
    return True


def __saveTweet(text, schedule = None):
    # saving tweet to database
    timestamp = datetime.now()
    tweet = Tweet(user_id, text, timestamp, scheduled_for = schedule)
    id = Database.saveTweet(tweet)
    return id


def __updateTweet(response, id):
    if response is not None:
        print("Tweet posted.")
        Database.updateTweetIdAndTime(id, response.id_str, datetime.now())  # adds tweet_id to db
    else:
        # error, delete tweet from database, rollback
        Database.deleteTweet(id)
        return False


def __checkDateTime(d8time):
    # datetime is a string YYYY-MM-DD@hh:mm
    if len(d8time) == 0 or len(d8time.split("@")) == 0:
        return False  # invalid format
    d8, taim = d8time.split('@')
    if len(d8.split("-")) != 3 or len(taim.split(":")) != 2:
        return False  # invalid format
    d8 = d8.split("-")
    taim = taim.split(":")
    try:
        d8time = datetime(year=int(d8[0]), month=int(d8[1]), day=int(d8[2]), hour=int(taim[0]), minute=int(taim[1]))
        if d8time <= datetime.now():
            return False  # old datetime
    except ValueError:
        return False  # invalid datetime
    return d8time
