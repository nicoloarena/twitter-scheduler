# contains methods for database interaction
import mysql.connector
import json
from datetime import datetime

from models.Tweet import Tweet


def connect():
    config_file = open("config.json")
    config = json.load(config_file)
    config_file.close()
    return mysql.connector.connect(
        database=config["db_data"],
        host=config["db_host"],
        user=config["db_user"],
        password=config["db_pass"]
    )


def __insert(query, values):
    mysql = connect()
    mycursor = mysql.cursor()
    mycursor.execute(query, values)
    mysql.commit()
    value = mycursor.lastrowid
    mycursor.close()
    mysql.close()
    return value


def __select(query, values, associative=False):
    mysql = connect()
    mycursor = mysql.cursor(dictionary=associative)
    mycursor.execute(query, values)
    returnValues = mycursor.fetchall()
    mycursor.close()
    mysql.close()
    return returnValues


def getTweet(id):
    query = "SELECT * FROM Tweet WHERE id = %s"
    values = (id,)
    tweet = __select(query, values, True)[0]
    return Tweet(author_id = tweet["author_id"], text = tweet["text"], timestamp = tweet["timestamp"],
                 id = tweet["id"], tweet_id = tweet["tweet_id"], created_at = tweet["created_at"],
                 scheduled_for = tweet["scheduled_for"])


def saveTweet(tweet):
    query = "INSERT INTO Tweet VALUES (NULL, %s, %s, %s, %s, %s, %s)"
    values = (tweet.getTweetId(), tweet.getAuthorId(), tweet.getText(), tweet.getTimestamp(),
              tweet.getCreatedAt(), tweet.getScheduledFor())
    return __insert(query, values)


def updateTweetIdAndTime(id, tweet_id, created_at):
    query = "UPDATE Tweet SET tweet_id = %s, created_at = %s WHERE id = %s"
    values = (tweet_id, created_at, id)
    __insert(query, values)
    return True


def deleteTweet(id):
    query = "DELETE FROM Tweet WHERE id = %s"
    values = (id,)
    __insert(query, values)
    return True


def saveUser(user):
    query = "INSERT INTO User VALUES(%s, %s, %s) ON DUPLICATE KEY UPDATE screen_name = %s, name = %s"
    values = (user.getId(), user.getScreenName(), user.getName(), user.getScreenName(), user.getName())
    __insert(query, values)
    return True


def getScheduledTweets(author_id, past = True):
    if past:
        sign = "<="
    else:
        sign = ">"
    query = "SELECT * FROM Tweet WHERE tweet_id IS NULL AND author_id = %s AND scheduled_for " + \
            sign + " %s ORDER BY scheduled_for ASC LIMIT 200"
    values = (author_id, datetime.now(),)
    scheduled = __select(query, values, True)
    tweets = []
    for tweet in scheduled:
        tweets.append(Tweet(author_id = tweet["author_id"], text = tweet["text"],
                            timestamp = tweet["timestamp"], id = tweet["id"],
                            tweet_id = tweet["tweet_id"], created_at = tweet["created_at"],
                            scheduled_for = tweet["scheduled_for"]))
    return tweets
