# Twitter Scheduler
This cli app allows users to handle personal Tweets.
#### Commands:
- `login` : use it to login to your twitter account. You have to authorize the app in the web browser and insert the pin here;
- `post <"text">` : use it to create a new Tweet. You can insert #hashtags and @mentions, and add lines using \\n as newline character. The text must be enclosed in double quotes, his max length is 280 characters and it cannot be equal to the last Tweet posted. Add --datetime option to schedule the Tweet. You can only post 300 Tweets during a 3 hour period;
- `posted` : use it to view the most recent 200 posted tweets. Tweets are ordered by posting date and time, in descending order;
- `scheduled`: use it to view the closest 200 scheduled tweets. Tweets are ordered by scheduling date and time, in ascending order.
#### Options:
- `--datetime <YYYY-MM-DD@hh:mm>` : specify when the tweet will be posted. In order: year, month, day, @, hour, minute. You have to keep the app running and be logged in order to post scheduled tweets.

## Configuration

#### Prerequisites:
You have to install the following tools:
- python 3.8, pipenv, pip;
- mysql

### Database structure
The database schema is dumped in `schema.sql` file. After completing database setup you have to put info, regarding user, password, database name and host, inside `config.json`.

#### E-R Diagram
![E-R Diagram](images/er.png)

#### Logic Schema
![Logic Schema](images/logic.png)


### Python Libraries
You have to create a pipenv environment and install `tweepy`, `mysql-connector-python`, and all the libraries specified in `Pipfile`.

### Twitter
You have to apply for a developer twitter account and after that create an application. Then you have to get consumer_key, consumer_secret and token from your application settings and put them in `config.json`.
