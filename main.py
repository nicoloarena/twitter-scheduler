# menu handling, calls methods from Commands.py
import Commands
import shlex
import threading

import logging

commands = {
    "login": "login",
    "post": "post",
    "scheduled": "scheduled",
    "posted": "posted",
    "help": "--help"
}

options = {
    "schedule": "--datetime",
    "verbose": "--verbose"
}


def main():
    logging.basicConfig(filename=".log", level=logging.DEBUG, format='%(asctime)s %(levelname)s : %(message)s')

    # starting daemon for scheduled tweets
    try:
        threading.Thread(target=Commands.postScheduledDaemon, daemon=True).start()
    except:
        print("Error: unable to start daemon thread for scheduled tweets.")

    try:
        while True:
            text = input()
            text = shlex.split(text)  # doesn't split inside double quotes
            return_value = True
            if len(text) == 0:
                continue
            elif text[0] == commands["login"]:
                # login
                return_value = Commands.login(text)
            elif text[0] == commands["post"]:
                # post or schedule tweet
                return_value = Commands.postTweet(text)
            elif text[0] == commands["scheduled"]:
                # retrieve scheduled tweets in the future
                return_value = Commands.getFutureScheduled(text)
            elif text[0] == commands["posted"]:
                # retrieve posted tweets
                return_value = Commands.retrieveTweets(text)
            elif text[0] == commands["help"]:
                # show help text
                return_value = Commands.support()
            else:
                print("Error: command not found. Use --help to show instructions")
            if return_value is False:
                print("Use --help to show instructions")
    except KeyboardInterrupt:
        print("\nbye")


if __name__ == "__main__":
    main()
